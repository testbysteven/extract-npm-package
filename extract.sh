#!/bin/sh

NPM_REGISTRY='@testbysteven:registry=https://gitlab.com/api/v4/packages/npm'
grep -qxF $NPM_REGISTRY .npmrc || echo $NPM_REGISTRY >> .npmrc

PACKAGE=$(npm view @testbysteven/default-webpack dist.tarball)

mkdir -p ./tmp/extract && \
curl -sS "$PACKAGE" > ./tmp/extract/package.zip
tar -xzf ./tmp/extract/package.zip && \
cp -R ./package/ ./
rm -rf ./tmp ./package
