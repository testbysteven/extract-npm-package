#!/bin/sh

SCOPE='testbysteven'
PACKAGE="default-webpack"
NPM_REGISTRY="@$SCOPE:registry=https://gitlab.com/api/v4/packages/npm"
grep -qxF $NPM_REGISTRY .npmrc || echo $NPM_REGISTRY >> .npmrc

VERSION=$(npm view "@$SCOPE/$PACKAGE" dist-tags.latest)

npm pack "@$SCOPE/$PACKAGE" && \
tar -xzf "./$SCOPE-$PACKAGE-$VERSION.tgz" && \
cp -R ./package/ ./ && \
rm -rf ./package "./$SCOPE-$PACKAGE-$VERSION.tgz"
