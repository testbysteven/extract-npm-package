Give some permission to the extraction file:
```
sudo chmod 755 extract.sh
sudo chmod 755 extract-only-npm.sh
```

run the extract.sh or extract-only-npm.sh script
```
./extract.sh
```
or
```
./extract-only-npm.sh
```

Enjoy your extracted package
